window.onload = function () {
    createLine(actualPath);
};

function createLine(text, color="white", tagName="P") {
    let btn = document.createElement(tagName);
    btn.innerHTML = text;
    btn.className = "text";
    btn.style.color = color;
    document.body.appendChild(btn);
    window.scroll(0, window.innerHeight);
}