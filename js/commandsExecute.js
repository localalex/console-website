
function execute(text) {
    console.log(new Date());
    let elements = text.split("\\");
    let temp = text.replace("&gt;", '>');
    let command = temp.replace(actualPath, '');
    command = command.split(" ");
    console.log(paths);

    switch (command[0].toLowerCase())
    {
        case "help":
            help(command);
            break;
        case "ls":
            ls(command);
            break;
        case "dir":
            ls(command);
            break;
        case "cat":
            cat(command);
            break;
        case "more":
            cat(command);
            break;
        case "type":
            cat(command);
            break;
        case "cd":
            cd(command);
            break;
        case "clear":
            clear();
            break;
        case "cls":
            clear();
            break;
        case "mkdir":
            mkdir(command);
            break;
        case "touch":
            touch(command);
            break;
        case '':
            break;
        default:
            error("This command doesn't exist. help to get all available commands");
            break;
    }

    if (command[0] !== '' && command[0] !== "clear" && command[0] !== "cls")
    {
        createLine("<br>");
    }
}

function touch(command) {
    if (command.length > 2 || command.length === 1)
    {
        error("touch [folder]");
    }
    else
    {
        let pathStructure = getActualPathStructure();
        if (pathStructure[pathStructure.length - 1] === '')
        {
            pathStructure.pop();
        }

        let value = {};
        value["type"] = "file";
        value["time"] = getFormattedTime();
        value["content"] = {};

        setByPath(paths, pathStructure, value, command[1]);
        savePathsLocally();
    }
}

function mkdir(command) {
    if (command.length > 2 || command.length === 1)
    {
        error("mkdir [folder]");
    }
    else
    {

        let pathStructure = getActualPathStructure();
        if (pathStructure[pathStructure.length - 1] === '')
        {
            pathStructure.pop();
        }

        let value = {};
        value["type"] = "folder";
        value["time"] = getFormattedTime();
        value["content"] = {};

        setByPath(paths, pathStructure, value, command[1]);
        savePathsLocally();
    }
}

function cd(command) {
    if (command.length > 2 || command.length === 1)
    {
        error("cd [path]");
    }
    else
    {
        let targetPath = command[1];

        if (targetPath === "..")
        {
            let pathStructure = getActualPathStructure();

            if(pathStructure.length > 1)
            {
                pathStructure.pop();
                if (pathStructure.length === 1)
                {
                    actualPath = pathStructure.join("\\") + "\\>";
                }
                else
                {
                    actualPath = pathStructure.join("\\") + ">";
                }
            }
        }
        else if (targetPath === ".")
        {

        }
        else
        {
            let pathStructure = getActualPathStructure();
            if (pathStructure[pathStructure.length - 1] === '')
            {
                pathStructure.pop();
            }
            let {level, keys} = getActualPathLevelAndKeys(pathStructure);
            if (keys.includes(targetPath))
            {
                if (level[targetPath].type === "folder")
                {
                    pathStructure.push(targetPath);
                    actualPath = pathStructure.join("\\") + ">";
                }
                else
                {
                    error("'" + targetPath + "' isn't a folder.");
                }
            }
            else
            {
                error("Can't find a folder named '" + targetPath + "'");
            }
        }
    }
    saveActualPathLocally();
}

function ls(command) {
    if (command.length > 1)
    {
        error("dir or ls");
    }
    else
    {
        let pathStructure = getActualPathStructure();
        if (pathStructure[pathStructure.length - 1] === '')
        {
            pathStructure.pop();
        }
        let {level, keys} = getActualPathLevelAndKeys(pathStructure);

        for (const key of keys) {
            let element = level[key];
            let text = "";
            let color = "lightgreen";
            if (element.type === "folder")
            {
                text = element.time + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "&lt;DIR&gt;" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + key;
            }
            else
            {
                text = element.time + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  key;
            }

            createLine(text, color);
        }
    }
}

function cat(command) {
    if (command.length > 2 || command.length === 1)
    {
        error("cat [file], more [file], type [file]");
    }
    else
    {
        let fileName = command[1];

        let pathStructure = getActualPathStructure();
        if (pathStructure[pathStructure.length - 1] === '')
        {
            pathStructure.pop();
        }
        let {level, keys} = getActualPathLevelAndKeys(pathStructure);

        if (keys.includes(fileName))
        {
            if (level[fileName].type === "file")
            {
                createLine(level[fileName].content, "lightskyblue", "P")
            }
            else
            {
                error("'" + fileName + "' isn't a file.");
            }
        }
        else
        {
            error("Can't find a file named '" + fileName + "'");
        }

    }
}

function help(command) {

    if (command.length > 2)
    {
        error("help (command)");
    }
    else
    {
        let target = "";
        if(command.length === 2)
        {
            target = command[1];
        }

        let color = "lightgreen";
        if (target === "")
        {
            for (const commandKey in commands) {
                createLine(commandKey + " : " + commands[commandKey], color);
            }
        }
        else
        {
            if (Object.keys(commands).includes(target))
            {
                createLine(target + " : " + commands[target], color);
            }
            else
            {
                error("This command doesn't exist. help to get all available commands");
            }
        }
    }

}

function clear() {
    while (document.body.lastElementChild) {
        document.body.removeChild(document.body.lastElementChild);
    }
}

function error(text) {
    createLine(text, "lightcoral");
}
