document.onkeydown = function (e) {
    e = e || window.Event;
    let target = document.body.getElementsByTagName("P")[document.body.getElementsByTagName("P").length - 1];
    if (e.code === "Enter")
    {
        execute(target.innerHTML);
        createLine(actualPath);
    }
    else if ("ABCDEFGHIJLKMNOPQRSTUVWXYZabcdefghijlkmnopqrstuvwxyz0123456789 .-_/".includes(e.key))
    {
        target.innerHTML += e.key;
    }
    else if (e.code === "Backspace")
    {
        if (target.innerHTML.length > actualPath.length)
        {
            target.innerHTML = target.innerHTML.removeCharAt(target.innerHTML.length);
        }
    }
};

