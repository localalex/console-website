let actualPath;
let paths;


if (localStorage.getItem("actualPath") !== null)
{
    actualPath = localStorage.getItem("actualPath");
}
else
{
    actualPath = "C:\\Users\\Anas>";
}

if (localStorage.getItem("paths") !== null)
{
    paths = JSON.parse(localStorage.getItem("paths"));
}
else
{
    paths = {
        "C:":
            {
                "type": "folder",
                "content":
                    {
                        "Users":
                            {
                                "type": "folder",
                                "time": "22/09/2021  23:24",
                                "content":
                                    {
                                        "Anas":
                                            {
                                                "type": "folder",
                                                "time": "08/12/2021  20:52",
                                                "content":
                                                    {
                                                        "About.me": {
                                                            "type": "file",
                                                            "time": "08/12/2021  21:03",
                                                            "content": /*TODO*/"Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus"
                                                        },
                                                        "contact.me": {
                                                            "type": "file",
                                                            "time": "06/12/2021  05:40",
                                                            "content": ""//TODO
                                                        },
                                                        "projects": {
                                                            "type": "folder",
                                                            "time": "09/12/2021  20:39",
                                                            "content": {
                                                                //TODO
                                                            }
                                                        },
                                                    }
                                            }
                                    }

                            },
                        "secret":
                            {
                                "type": "folder",
                                "time": "22/09/2021  23:07",
                                "content":
                                    {
                                        //TODO
                                    }
                            }
                    }
            }
    };
}


let commands = {
    "cd [path]": "Shows information about commands.",
    "cat [file]": "Shows the content of a file.",
    "clear": "Clears the screen",
    "cls": "Clears the screen",
    "dir (folder)": "Shows the content of a folder.",
    "help (command)": "Shows information about commands.",
    "ls (folder)": "Shows the content of a folder.",
    "more [file]": "Shows the content of a file.",
    "mkdir [name]": "Creates a directory",
    "touch [name]": "Creates a file",
    "type [file]": "Shows the content of a file."
}