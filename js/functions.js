//from stackoverflow
String.prototype.removeCharAt = function (i) {
    let tmp = this.split('');
    tmp.splice(i - 1 , 1);
    return tmp.join('');
}

//from stackoverflow
function textWidth(text, fontProp) {
    let tag = document.createElement('div')
    tag.style.position = 'absolute'
    tag.style.left = '-99in'
    tag.style.whiteSpace = 'nowrap'
    tag.style.font = fontProp
    tag.innerHTML = text

    document.body.appendChild(tag)
    let result = tag.clientWidth
    document.body.removeChild(tag)
    return result;
}

function pathExists(path) {

}

function getActualPathStructure() {
    let pathStructure = actualPath.split("\\");
    pathStructure[pathStructure.length - 1] = pathStructure[pathStructure.length - 1].replace(">", "")
    return pathStructure;
}

function getActualPathLevelAndKeys(pathStructure) {
    let level = paths;
    for (const key of pathStructure) {
        if (key !== '') {
            if (level[key].type === "folder") {
                level = level[key].content;
            }
        }
    }
    let keys = Object.keys(level);
    return {level, keys};
}

function setByPath(obj, path, value, name) {
    let level = obj;
    for (const key of path) {
        if (key !== '') {
            if (level[key].type === "folder") {
                level = level[key].content;
            }
        }
    }
    level[name] = value;
}

function saveActualPathLocally() {
    localStorage.setItem("actualPath", actualPath);
}

function savePathsLocally() {
    localStorage.setItem("paths", JSON.stringify(paths));
}

function getFormattedTime() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    let h = today.getHours();
    let m = today.getMinutes();

    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    return dd + '/' + mm + '/' + yyyy + "  " + h + ":" + m;
}